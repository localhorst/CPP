#include <iostream>
#include <string.h>

using namespace std;

void ausgabe(char* p[], int max);
void freeArray(char* p[], int max);

int main(int argc, char** argv) {

	printf("Anzahl der Parameter %i\n\n", (argc - 1));

	char* cpArray[(argc - 1)];

	argv++;

	for (int i = 0; i < (argc - 1); ++i) {

		printf("Übergabe: %s - ", *argv);

		int size = strlen(*argv);
		printf("Laenge des Parameter %i\n", size);

		cpArray[i] = (char*) malloc((size - 1) * sizeof(char*));

		strcpy(cpArray[i], *argv);

		argv++;
	}

	free(*argv);

	ausgabe(cpArray, argc - 1);

	freeArray(cpArray, argc - 1);

	ausgabe(cpArray, argc - 1);

	cout << endl;
	return 0;
}

void ausgabe(char* p[], int max) {

	cout << endl;

	for (int i = 0; i < max; ++i) {
		printf("Ausgabe: %s\n", p[i]);
	}

}

void freeArray(char* p[], int max) {

	cout << endl << "Freigeben" << endl;

	for (int i = 0; i < max; ++i) {
		free(p[i]);
	}
}
