/*
 * C1.cpp
 *
 *  Created on: 03.05.2018
 *      Author: hendrik
 */

#include <iostream>
using namespace std;

unsigned int app(unsigned short int us1, unsigned short int us2) {

	unsigned int ret = us1;
	ret = ret << 8;
	ret = ret + us2;
	return ret;
}

int main() {
	unsigned int (*appPointer)(unsigned short int, unsigned short int);
	appPointer = app;
	unsigned short int us1 = 10;
	unsigned short int us2 = 10;
	printf("us1: %x\n", us1);
	printf("us2: %x\n", us2);
	printf("Ergebnis: %04x ", appPointer(us1, us2));
	return 0;
}
