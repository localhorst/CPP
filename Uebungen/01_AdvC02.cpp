/*
 * C2.cpp
 *
 *  Created on: 03.05.2018
 *      Author: hendrik
 */

#include <iostream>
using namespace std;

void f1(unsigned int i);
void f2(unsigned int i);
void f3(unsigned int i);

void f1(unsigned int i) {
	printf("Funktion 1: %i\n", i);
}

void f2(unsigned int i) {
	printf("Funktion 2: %i\n", i);
}

void f3(unsigned int i) {
	printf("Funktion 2: %i\n", i);
}


int main() {

	int uiselected = 1;
	void (*f_array[])(unsigned int) = {f1, f2, f3};

	unsigned int params[] = {100, 2, 50};

	f_array[uiselected-1] (params[uiselected]);

	return 0;
}

