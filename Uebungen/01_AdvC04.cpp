
#define MAX(a, b) ((a) < (b) ? (b) : (a))

#include <iostream>
using namespace std;

int main() {

	int iwert2 = 1000;
	unsigned int uiwert2 = -1500;
	printf("Maximum: %d\n", MAX(iwert2, uiwert2));
// --> nicht TypeSafe

	return 0;
}
