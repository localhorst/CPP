/*
 * Main.cpp
 *
 *  Created on: 07.06.2018
 *      Author: hendrik
 */

#include <iostream>
using namespace std;

void callByReferenz(int* pi) {
	*pi = 10;
}

namespace na01 {
int zahl = 20;
}

namespace na02 {
int zahl = 30;
}

void defaultValues(int x = 2, int y = 4) {
	printf("X: %i - Y: %i\n", x, y);
}

void func() {
	printf("01\n");
}

void func(int i) {
	printf("02: %i\n", i);
}

template<typename T>
T add(T x, T y) {
	T value;
	value = x + y;
	return value;
}

int main(int argc, char *argv[]) {

	printf("Hallo Welt\n");

	int i = 0;
	callByReferenz(&i);
	printf("Zahl: %i\n", i);

	printf("Namespace 01: %i\n", na01::zahl);
	printf("Namespace 02: %i\n", na02::zahl);

	defaultValues();
	defaultValues(8);
	defaultValues(12, 16);

	func();
	func(2);

	printf("tmp01 %i\n" , add(8,9));
	printf("tmp01 %f\n" , add(1.259,8985.5));

	return 0;
}

