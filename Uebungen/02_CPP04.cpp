#include <iostream>
using namespace std;

class Shape {
protected:
	static int inumberofinstances;
public:
	static void printInstanzen() {
		printf("Instanzen %i \n", Shape::inumberofinstances);
	}

	virtual void draw() {
	}

	virtual ~Shape() {
	}

	Shape() {
	}
};

class Triangle: public Shape {
public:
	void draw() {
		printf("Zeichne Triangle \n");
	}
	~Triangle() {
		printf("Zerstoere Triangle\n");
		Shape::inumberofinstances--;
	}

	Triangle() {
		printf("Erstelle Triangle\n");
		inumberofinstances++;
	}

};

class Circle: public Shape {
public:
	void draw() {
		printf("Zeichne Circle \n");
	}
	~Circle() {
		printf("Zerstoere Circle\n");
		Shape::inumberofinstances--;

	}

	Circle() {
		printf("Erstelle Circle\n");
		Shape::inumberofinstances++;
	}

};

class Rectangle: public Shape {
public:
	void draw() {
		printf("Zeichne Rectangle \n");
	}
	~Rectangle() {
		printf("Zerstoere Rectangle\n");
		Shape::inumberofinstances--;
	}

	Rectangle() {
		printf("Erstelle Rectangle\n");
		Shape::inumberofinstances++;
	}

};

int Shape::inumberofinstances = 0;

int main(int argc, char *argv[]) {

	Triangle* tri = new Triangle();
	Circle* cir = new Circle();
	Rectangle* rec = new Rectangle();
	printf("\n");

	Shape* shapes[10] = { };

	shapes[0] = tri;
	shapes[1] = cir;
	shapes[2] = rec;

	shapes[0]->draw();
	shapes[1]->draw();
	shapes[2]->draw();

	Shape* shp = new Shape();
	shp->printInstanzen();

	printf("\n");

	tri->~Triangle();
	cir->~Circle();
	rec->~Rectangle();

	printf("\n");

	shp->printInstanzen();

	return 0;
}

