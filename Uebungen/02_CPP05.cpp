/*
 * 02_CPP05.cpp
 *
 *  Created on: 17.06.2018
 *      Author: hendrik
 */
#include <iostream>
#include <math.h>
using namespace std;

class Complex {

private:
	double real;
	double img;

public:
	Complex(double real, double img) {
		this->real = real;
		this->img = img;
	}

	Complex(const Complex& c) {
		this->real = c.real;
		this->img = c.img;
	}

	void print() {
		cout << real << " + j" << img << "\n";
	}

	Complex operator+(const Complex& c) {
		Complex temp(0, 0);
		temp.real = real + c.real;
		temp.img = img + c.img;
		return temp;
	}

	Complex operator-(const Complex& c) {
		Complex temp(0, 0);
		temp.real = real - c.real;
		temp.img = img - c.img;
		return temp;
	}

	Complex operator*(const Complex& c) {
		Complex temp(0, 0);
		temp.real = (real * c.real) - (img * c.img);
		temp.img = (real * c.img) + (c.real * img);
		return temp;
	}

	Complex operator/(const Complex& c) {
		Complex temp(0, 0);
		double div = (pow(c.real, 2)) + (pow(c.img, 2));
		temp.real = ((real * c.real + img * c.img) / div);
		temp.img = ((img * c.real - real * c.img) / div);

		return temp;
	}

	Complex operator!() {
		Complex temp(0, 0);
		temp.real = (real) / (pow(real, 2)) + (pow(img, 2));
		temp.img = (img) / (pow(real, 2)) + (pow(img, 2));
		return temp;
	}

	friend ostream& operator<<(ostream& ostr, const Complex& c) {
		ostr << c.real << "+j" << c.img;
		return ostr;
	}

};

int main() {

	Complex R1 = Complex(50, 0);
	Complex R2 = Complex(50, 0);
	Complex R3 = Complex(100, 0);
	Complex R4 = Complex(200, 0);

	Complex XC1 = Complex(0, -100);
	Complex XC2 = Complex(0, -100);

	Complex XL1 = Complex(0, 100);
	Complex XL2 = Complex(0, 100);
	Complex XL3 = Complex(0, 50);

	Complex Z = Complex(0, 0);

	Complex A = Complex(0, 0);
	Complex B = Complex(0, 0);
	Complex C = Complex(0, 0);
	Complex D = Complex(0, 0);

	Complex E = Complex(0, 0);
	Complex F = Complex(0, 0);

	A = (XL1 + R1);
	B = (XC1 + R2);

	C = (XC2 + R3 + XL3);
	D = (XL2 + R4);

	E = (A * B) / (A + B);
	F = (C * D) / (C + D);

	Z = E + F;

	Z.print();

	return 1;

}

