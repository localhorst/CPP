#include <iostream>
using namespace std;

#define ERROR_RINGPUFFER_EMPTY -1
#define ERROR_RINGPUFFER_FULL -2
#define RINGPUFFER_SIZE 8

class ringpuffer {

private:
	unsigned int uispos; //schreib
	unsigned int uirpos; //lese
	unsigned int uinumelements;
	int iArray[RINGPUFFER_SIZE];

public:
	ringpuffer() {
		cout << "Init\n" << endl;
		uispos = 0;
		uirpos = 0;
		uinumelements = 0;
	}

	int storeelement(int ielement) {

		if ((uispos + 1 == uirpos)
				|| (uirpos == 0 && uispos + 1 == uinumelements)) {
			printf("ERROR_RINGPUFFER_FULL\n");
			return ERROR_RINGPUFFER_FULL;
		}

		iArray[uispos] = ielement;
		uinumelements++;

		uispos++;

		if (uispos >= RINGPUFFER_SIZE) {
			uispos = 0;
		}
		return 0;
	}
	int readelement(int& ielement) {

		if ((uirpos == uispos) && (uinumelements == 0)) {
			printf("ERROR_RINGPUFFER_EMPTY\n");
			return ERROR_RINGPUFFER_EMPTY;
		}
		ielement = iArray[uirpos];
		uinumelements--;
		uirpos++;

		if (uirpos >= RINGPUFFER_SIZE) {
			uirpos = 0;
		}
		return 0;
	}

	void printArray() {
		cout << "Print Array" << endl;
		for (unsigned int i = 0; i < RINGPUFFER_SIZE; i++) {
			printf("Index: %i --> %i", i, iArray[i]);
			if (i == uirpos) {
				printf(" --> Lesezeiger");
			}
			if (i == uispos) {
				printf(" --> Schreibzeiger");
			}

			printf("\n");

		}
		cout << endl;
	}
};

int main() {

	ringpuffer* rb = new ringpuffer();
	int res;
	int* i = &res;

	rb->storeelement(1);
	rb->storeelement(2);
	rb->storeelement(3);
	rb->storeelement(4);

	rb->printArray();

	rb->readelement(*i);
	printf("Read: %i\n\n", res); //1
	rb->readelement(*i);
	printf("Read: %i\n\n", res); //2
	rb->readelement(*i);
	printf("Read: %i\n\n", res); //3
	rb->readelement(*i);
	printf("Read: %i\n\n", res); //4

	rb->printArray();

	rb->storeelement(8);
	rb->storeelement(1);
	rb->storeelement(2);
	rb->storeelement(3);
	rb->storeelement(4);
	rb->storeelement(1);
	rb->storeelement(2);

	rb->readelement(*i);
	printf("Read: %i\n\n", res); //8

	rb->printArray();

}
