#include <iostream>
using namespace std;

#define ERROR_RINGPUFFER_EMPTY -1
#define ERROR_RINGPUFFER_FULL -2
#define RINGPUFFER_SIZE 8

class ringpuffer {

private:
	unsigned int uispos; //schreib
	unsigned int uirpos; //lese
	unsigned int uinumelements;
	int iArray[RINGPUFFER_SIZE];

public:
	ringpuffer() {
		cout << "Init\n" << endl;
		uispos = 0;
		uirpos = 0;
		uinumelements = 0;
	}

	int storeelement(int ielement) {

		if ((uispos + 1 == uirpos)
				|| (uirpos == 0 && uispos + 1 == uinumelements)) {
			printf("ERROR_RINGPUFFER_FULL\n");
			return ERROR_RINGPUFFER_FULL;
		}

		iArray[uispos] = ielement;
		uinumelements++;

		uispos++;

		if (uispos >= RINGPUFFER_SIZE) {
			uispos = 0;
		}
		return 0;
	}
	int readelement(int& ielement) {

		if (uirpos == uispos) {
			printf("ERROR_RINGPUFFER_EMPTY\n");
			return ERROR_RINGPUFFER_EMPTY;
		}
		ielement = iArray[uirpos];
		uinumelements--;
		uirpos++;

		if (uirpos >= RINGPUFFER_SIZE) {
			uirpos = 0;
		}
		return 0;
	}

	void printArray() {
		cout << "Print Array" << endl;
		for (unsigned int i = 0; i < RINGPUFFER_SIZE; i++) {
			printf("Index: %i --> %i", i, iArray[i]);
			if (i == uirpos) {
				printf(" --> Lesezeiger");
			}
			if (i == uispos) {
				printf(" --> Schreibzeiger");
			}

			printf("\n");

		}
		cout << endl;
	}
};

class IOChannel {

private:

	ringpuffer rpA[3];

public:

	IOChannel() {
		//rpA[0] = new ringpuffer();
	}

	int storeelement(unsigned int uichannel, int ielement) {
		return rpA[uichannel].storeelement(ielement);

	}

	int readelement(unsigned int uichannel, int& ielement) {
		return rpA[uichannel].readelement(ielement);
	}

};

int main() {
	int res;
	int* i = &res;

	IOChannel* ioc = new IOChannel();

	ioc->storeelement(0, 5);
	ioc->storeelement(1, 10);
	ioc->storeelement(2, 15);

	ioc->readelement(0, res);
	printf("Read: %i\n\n", res); //5

	ioc->readelement(2, res);
	printf("Read: %i\n\n", res); //15

	ioc->readelement(1, res);
	printf("Read: %i\n\n", res); //10

}
